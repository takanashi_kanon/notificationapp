package jp.kanon.takanasi.nofiticationapp;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

public class IntentActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("DEBUG", "開始");
        Intent openedIntent = getIntent();
        Log.d("DEBUG", "openedIntent" + openedIntent);

        if (openedIntent != null) {
            if (Intent.ACTION_SEND.equals(openedIntent.getAction())) {

                CharSequence uri = openedIntent.getExtras().getCharSequence(Intent.EXTRA_TEXT);

                Log.d("DEBUG", "uri" + uri);
                Log.d("DEBUG", "title:" + openedIntent.getStringExtra("title"));

                if (uri != null) {
                    Nofitication(Uri.parse(uri.toString()));
                }
            }
        }
        Log.d("DEBUG", "finish?");
        this.finish();
    }

    private void Nofitication(Uri obtainUri) {

        //ビルダーを取得
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

        //適当なアイコンをセット
        builder.setSmallIcon(R.mipmap.ic_launcher);

        //HTMLのタイトルかアプリ名を入れたい
        builder.setContentTitle("title"); // 1行目
        builder.setContentText(obtainUri.toString()); // 2行目

        //何に使うかなーこの三行目
        builder.setSubText("SubText"); // 3行目

        //アプリ名を入れておく
        builder.setContentInfo("notification app"); // 右端

        //ここまで
        //現在時刻を入れておく
        builder.setWhen(14000000); // タイムスタンプ（現在時刻、メール受信時刻、カウントダウンなどに使用）

        //いらない？ // 5.0からは表示されない
        builder.setTicker("Ticker"); // 通知到着時に通知バーに表示(4.4まで)

        //マネージャーの取得
        NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());

        //適当ID

        //Intentを取得したURLから取得
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setData(obtainUri);


        SharedPreferences data = getSharedPreferences("SavedNotification", Context.MODE_PRIVATE);

        //インテントの設定
        Intent i = new Intent(
                getApplicationContext(),
                MainActivity.class);
        i.setAction(Intent.ACTION_MAIN);
        i.putExtra("URL", obtainUri.toString());

        //Preferenceにデータを保存しつつ、intで番号を返す。
        int NOTIFICATION_ID = CheckAndPutPreferentceData(data,obtainUri);
        i.putExtra("num", NOTIFICATION_ID);

        //たぶん、新しいPendingIntentを作らなければいけないっていう感じか。。。。...。。。つらい。
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), NOTIFICATION_ID,
                i, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        //ここで実際に作成
        Notification nofitication = builder.build();
        nofitication.flags = Notification.FLAG_NO_CLEAR; //消せなくする。

        //消す場合は manager.cancel(R.layout.activity); //消すためのIDは作るときに使った一意のID
        manager.notify(NOTIFICATION_ID, nofitication);
    }

    private int CheckAndPutPreferentceData(SharedPreferences data, Uri obtainUri) {

        SharedPreferences.Editor editor = data.edit();
        int i = 0;
        while (i < 20) {
            if (data.getString(String.valueOf(i), null) == null) {
                //その番号のところに保存
                editor.putString(String.valueOf(i), obtainUri.toString());
                editor.apply();
                break;
            }
            i++;
        }
        return i;
    }


}
