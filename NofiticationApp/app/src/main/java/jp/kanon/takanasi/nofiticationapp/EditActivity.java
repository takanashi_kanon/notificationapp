package jp.kanon.takanasi.nofiticationapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;


public class EditActivity extends Activity {


    EditText editText1 = null;
    EditText editText2 = null;
    Button button = null;

    SharedPreferences pref =null;

    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        setContentView(R.layout.edit_detail_layout);

        editText1 = (EditText)findViewById(R.id.editText1);
        editText2 = (EditText)findViewById(R.id.editText2);
        button = (Button)findViewById(R.id.enterButton);

        pref = getSharedPreferences("savedIntent",MODE_PRIVATE);


    }
}
