package jp.kanon.takanasi.nofiticationapp;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity{

    SharedPreferences prefs = null;
    ListView listView = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Intentを受け取る。//ここで受け取るintentは通知からのintent。
        Intent intent = getIntent();

        //intentからURLを抜き出してさらにURLを発行する。
        String urlString = intent.getStringExtra("URL");
        int num = intent.getIntExtra("num",-1);
        Log.d("DEBUG",""+urlString);
        Log.d("DEBUG ",""+ num);

        //URLだったら起動する。
        if(urlString != null && num != -1) {
            NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(num);
            StartURLOpenActivity(urlString);
        }
        //intentからアプリ選択を開く。
//        StartIntentChooser(urlString);


        //Preferenceからデータを取得。
        prefs = getSharedPreferences("SavedNotification",MODE_PRIVATE);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        //まー更新なんかにも使えるかも？
        SetupListViewData(prefs, adapter);


        //listviewを取得
        listView = (ListView)findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //URLを取得してIntentを飛ばす
                //ここにURLだったらっていう条件文入れたい。まぁいまはURLしかないのでこのまま。
                StartURLOpenActivity(parent.getItemAtPosition(position).toString());
//                StartIntentChooser(parent.getItemAtPosition(position).toString());
            }
        });

        //ロングタップ
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                Log.d("DEBUG","OnItemLongClick");
                //詳細兼編集ダイアログを開く。
                String title = "";
                String content = parent.getItemAtPosition(position).toString();
                ShowEditAndDetailDialog(title, content, position, adapter, prefs);

                return true; //タップを伝播しないよフラグ。
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }



    //URLからページを何かのアプリで開く。//受け取ったURLオブジェクト(String)に何か日本語などが入っている場合、
    //それをパースするみたいなのが必要なのでは。
    private void StartURLOpenActivity(String url)
    {

        Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(urlIntent);
        this.finish();
    }

    //ロングタップからダイアログを開く
    private void ShowEditAndDetailDialog(String title, final String detail,
         final int position, final ArrayAdapter<String> adapter, final SharedPreferences prefs){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(detail);

        builder.setPositiveButton("共有", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                StartIntentChooser(detail);
            }
        });

        builder.setNegativeButton("削除", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                //itemを削除したい。
                //Preferenceを削除したい。
                SharedPreferences.Editor editor= prefs.edit();
                editor.remove(String.valueOf(position));

                String item = (String) adapter.getItem(position);
                adapter.remove( adapter.getItem(position));
                adapter.notifyDataSetChanged();
                editor.commit();
            }
        });

        builder.setNeutralButton("編集", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                // 編集Acitivityを開く。
                // 特になんてことはないけれど、Preferenceを開いて、Preferenceに保存して、
                // MainActivityに戻ってきてまたonStartか何かで更新する処理が必要
                Intent intent = new Intent(getApplicationContext(), EditActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //urlを受け取って、IntentChooserを開く。
    private void StartIntentChooser(String text)
    {
        if(text != null )
        {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(sendIntent, "選択: 共有するアプリ"));
        }
    }

    private void SetupListViewData(SharedPreferences prefs, ArrayAdapter<String> adapter)
    {
        int i = 0;
        String itemString = prefs.getString(String.valueOf(i),"" );
        while(itemString != "")
        {
            itemString = prefs.getString(String.valueOf(i),"" );
            adapter.add(itemString);
            i++;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
